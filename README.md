# PROJECT 8: OPENCLASSROOMS FRONT-END PATH

**_Technical Documentation & Audit_**

## Overview

The Todo List application provided by OpenClassrooms is a simple SPA (Single Page Application) written using vanilla JavaScript without the use of any front-end frameworks. The application utilizes the Model View Controller architectural design pattern. Allowing the application to decouple its data from the view layer using a controller to manage the interactions between the two layers.

The main functionality provided by the application for its users includes the ability to:

-	Add new todo items
-	Edit existing todo items
-	Mark todo item(s) as completed
-	Remove completed todo items
-	Delete any todo item regardless of its status
-	Filter todo items based on active, completed, or neither status


## Dependencies

As the application is based on the famous TodoMVC project. It has following the dependencies from the TodoMVC project for it to work correctly.

-	`todomvc-common`: 1.0.1
-	`todomvc-app-css`: 2.0.1

Along with the Jasmine testing framework v2 required only during the development of the application.


## Application Structure

- The `/` root folder contains 2 important files.

  | File            | Description |
  | --------------- | ----------- |
  | `index.html`    | The main application page responsible for loading all of the needed resources like CSS and JavaScript files when the user visits the website. |
  | `package.json`  | List of all of the package dependencies used by the project. |


- The `/js` folder contains all of the important JavaScript files responsible for the app's logic.

  | File            | Description |
  | --------------- | ----------- |
  | `app.js`        | Creates a new instance of all of the required MVC and other components. |
  | `model.js`      | Contains functions needed to manage the data for the application. It’s responsible for the Model part of the MVC design pattern. |
  | `view.js`       | Contains needed functionality to allow the application to display data to the user visually. It’s responsible for the View part of the MVC design pattern. |
  | `controller.js` | Contains all actions required for the application, including linking the Model and the View layers together. It's responsible for the Controller part of the MVC design pattern. |
  | `store.js`      | The application doesn’t store its data online using an API. Instead, it uses a fake database created locally in the user’s browsers using localStorage. Which behaves like a persistent database. But, if the user or the application itself clears the cache, the database will be deleted. Taking all the user’s to-do items with it. |
  | `template.js`   | Contains HTML templates for the List elements along with miscellaneous related elements and HTML escape characters that help in reducing code duplication in our applications View layer. |
  | `helpers.js`    | Contains helper functions for interacting with the DOM. Helping reduce code verbosity and improving development experience. |

- The `/test` folder contains 2 files.

  | File                | Description |
  | ------------------- | ----------- |
  | `ControllerSpec.js` | Contains all of the unit tests written using the Jasmine testing framework for this application. |
  | `SpecRunner.html`   | Runs the unit tests written in the ControllerSpec.js file for the project and shows the results for them. |

- The `/node_modules` folder contains all of the external dependence's used by the application.

  | File              | Description |
  | ----------------- | ----------- |
  | `jasmine-core`    | Allows us to write unit tests using the Jasmine framework to test the application. |
  | `todomvc-app-css` | Contains common CSS styles inherited from the original TodoMVC application to be used in the project. |
  | `todomvc-common`  | Provides helper methods used by the application under the hood for rendering view templates, etc. |


## Unit Tests

Upon receiving the project, the following tests were defined but not implemented. Which has been corrected by implementing them.

1. Should show entries on start-up
    ```javascript
    it('should show entries on start-up', function () {
      const todo = {title: 'my todo'};
      setUpModel([todo]);

      subject.setView('');

      expect(view.render).toHaveBeenCalledWith('showEntries', [todo]);
    });
    ```
2. Should show active entries
    ```javascript
    it('should show active entries', function () {
      const todo = {title: 'my todo'};
      setUpModel([todo]);

      subject.setView('#/active');

      expect(view.render).toHaveBeenCalledWith('showEntries', [todo]);
    });
    ```

3. Should show completed entries
    ```javascript
    it('should show completed entries', function () {
      const todo = {title: 'my todo'};
      setUpModel([todo]);

      subject.setView('#/completed');

      expect(view.render).toHaveBeenCalledWith('showEntries', [todo]);
    });
    ```
4. Should highlight "All" filter by default
    ```javascript
    it('should highlight "All" filter by default', function () {
      const todo = {id: 42, title: 'my todo', completed: false};
      setUpModel([todo]);

      subject.setView('');

      expect(view.render).toHaveBeenCalledWith('setFilter', '');
    });
    ```
5. Should toggle all todos to completed
    ```javascript
    it('should toggle all todos to completed', function () {
      const todo = {id: 42, title: 'my todo', completed: false};
      setUpModel([todo]);

      subject.setView('');
      view.trigger('toggleAll', {completed: true});

      expect(model.update).toHaveBeenCalledWith(42, {completed: true}, jasmine.any(Function));
    });
    ```
6. Should update the view
    ```javascript
    it('should update the view', function () {
      const todo = {id: 42, title: 'my todo', completed: false};
      setUpModel([todo]);

      subject.setView('');
      view.trigger('toggleAll', {completed: true});

      expect(view.render).toHaveBeenCalledWith('elementComplete', {id: 42, completed: true});
    });
    ```
7. Should add a new todo to the model
    ```javascript
    it('should add a new todo to the model', function () {
      setUpModel([]);

      const todoTitle = 'my todo';

      model.create(todoTitle, function(){});

      expect(model.create).toHaveBeenCalledWith(todoTitle, jasmine.any(Function));
    });
    ```
8. Should remove an entry from the model
    ```javascript
    it('should remove an entry from the model', function () {
      const todo = {id: 42, title: 'my todo', completed: false};
      setUpModel([todo]);

      model.remove(42, function(){});

      expect(model.remove).toHaveBeenCalledWith(42, jasmine.any(Function));
    });
    ```

While the following additional tests were added to help improve the application stability.

9. Should not show incomplete entries when showing completed entries
    ```javascript
    it('should not show incomplete entries when showing completed entries', function() {
      const todo = {title: 'my todo', completed: true};
      setUpModel([todo]);

      subject.setView('#/completed');
      expect(view.render).not.toHaveBeenCalledWith('showEntries', [{title: 'my todo', completed: false}]);
    });
    ```
10. Should highlight "Active" filter when switching to active view
    ```javascript
    it('should highlight "Active" filter when switching to active view', function () {
      const todo = {id: 42, title: 'my todo', completed: false};
      setUpModel([todo]);

      subject.setView('#/active');

      expect(view.render).toHaveBeenCalledWith('setFilter', 'active');
    });
    ```
11. Should not remove an entry from the model when the id is undefined
    ```javascript
    it('should not remove an entry from the model when the id is undefined', function () {
      const todo = {id: 42, title: 'my todo', completed: true};
      setUpModel([todo]);

      model.remove(42, function(){});

      expect(model.remove).not.toHaveBeenCalledWith(undefined, jasmine.any(Function));
    });
    ```

## Bugs

The application contained 2 major bugs that were discovered during its testing and fixed upon discovery.

### Bug #1

The first bug was the most severe as it broke the application and didn’t allow it to be usable at all. It was caused by a simple typo in the `/js/controller.js` file at line number **96**. The typo was the addition of an extra letter **"d"** in the function name for **"addItem"**.

Code with Typo:
```javascript
Controller.prototype.adddItem = function (title) {
```

Fixed Code:
```javascript
Controller.prototype.addItem = function (title) {
```

### Bug #2

The 2nd bug was found in the `/js/store.js` file starting at line number **77**. This bug allowed the application to save a todo item with a duplicate ID, potentially causing a conflict. When saving a new todo item, the function didn't check if the randomly generated ID for the todo item already existed in the database or not.

The effected section of the code for this issue:
```javascript
// Generate an ID
var newId = ""; 
var charset = "0123456789";

for (var i = 0; i < 6; i++) {
  newId += charset.charAt(Math.floor(Math.random() * charset.length));
}
```

Although, this issue could've been avoided in many ways. For example, by using a real database in the backend, or by using a 3rd-party library to generate UUID's or another mechanism. Instead of using a simple system of generating random numbers from a predefined list of numbers.

Though instead, the problem was fixed by refactoring the existing code and adding a check to make sure a duplicate ID would never be used even if it was generated. The solution for the duplicate ID bug from line number **83**:

```javascript
// Generate a unique ID, not already in use.
const _generateID = () => {
  let newId = ""; 
  const charset = "0123456789";

  for (let i = 0; i < 6; i++) {
    newId += charset.charAt(Math.floor(Math.random() * charset.length));
  }

  // Verify the new generated ID is not already used.
  let isIdDuplicate = false;

  for (let i = 0; i < todos.length; i++) {
    if (todos[i].id === newId) { isIdDuplicate = true; break; }
  }

  if (isIdDuplicate) {
    // Generate a new ID as the current one already exists by 
    // calling this function recursively, until a unique ID 
    // is generated.
    newId = _generateID();
  }

  return newId;
};
```

The `generateID` function being used in action at line number **124**:

```javascript
// Assign an ID
updateData.id = parseInt(_generateID());
```


## Audit

To make sense of the performance audit for the competitor’s website. We need to start by auditing our application so we can have a base reference point that we can compare the competitor’s website against. Although, the audit results for the two applications can’t be compared directly as they are quite different from each other. Even though our website was deployed to a server over the internet to reduce the number of differences between the two websites and provide them as close to an equal footing as possible. As such, the audit results should only be used as a general guide.

### Testing Environment

The audit for both applications was conducted on a Windows 10 system running a fully specced out Dell XPS 15 2018 laptop with 32 GB RAM and an Intel i7-8750H CPU. To minimize contamination from the browser, a build of Google Chrome Cannery (v82.0.4065.1) was used in incognito mode without consumer extensions interfering with the results. Each audit was conducted by clearing the local cache using the Chrome Developer tools.

### Our Application

#### App Load

The application loads quickly because of the website's small content size. And, using vanilla JavaScript over 3rd-party JavaScript libraries or frameworks along with having no dependencies on heavy assets such as images, fonts, videos, etc. Which would have increased the website load-time considerably.

Although, our application doesn’t load any assets such as the CSS and JS from external CDN servers. A step, that could help reduce the load times. Also, combining the different CSS and JS files into a single respective file for each type of content along with using GZip from the server-side will significantly improve the application load time.

The load tests were conducted under both the normal internet speed and a simulated slow 3G network speed. The result for both test cases is attached below.

**Average Results:**

- Application Size: 47.7 KB
- DOMContentLoaded: 1.43s
- Memory Heap: 1.3 MB

#### Page Load – Normal Network Speed

![](screenshots/own/01-Page-Load-Online-Network.png)

#### Page Load – Slow 3G Network Speed

![](screenshots/own/02-Page-Load-Slow-3G-Network.png)

#### Network – Normal Speed

![](screenshots/own/03-Network-Online.png)

#### Network – Slow 3G Speed

![](screenshots/own/04-Network-Slow-3G.png)

#### Memory Allocation

![](screenshots/own/05-Memory-Allocation.png)

#### Application Use

The general usage tests for the application show that the app continues to use fewer resources and memory while operating fast.

**The application loaded and executed all features in 4.46s:**

- Scripting: 7ms
- Rendering: 3ms

The application memory usage after 5 seconds of all features executed went from 1.3 MB to 911 KB. Which is extremely low.

**Adding a single task:**

- Scripting: 23ms
- Rendering: 8ms

![](screenshots/own/06-Adding-a-single-task.png)

**Marking a task to be completed:**

- Scripting: 42ms
- Rendering: 18ms

![](screenshots/own/07-Marking-a-task-to-be-completed.png)

**Marking multiple tasks to be completed:**

- Scripting: 18ms
- Rendering: 26ms

![](screenshots/own/08-Marking-multiple-tasks-to-be-completed.png)

**Clearing multiple completed tasks:**

- Scripting: 21ms
- Rendering: 8ms

![](screenshots/own/09-Clearing-multiple-completed-tasks.png)

**Navigating between all 3 filter views:**

- Scripting: 13ms
- Rendering: 21ms

![](screenshots/own/10-Navigating-all-3-filters-in-the-view.png)

### The Competitor’s App

The tests for the competitor’s app were done on the same Windows 10 PC with the same specs and environment used for auditing our app.

For reference, here is the [link to](http://todolistme.net/) the competitor's app.

#### Overview

The application provides a lot of features for helping in creating and managing several to-do lists. Allowing the user to schedule them and move them between different task lists. It even allows the user to reorder the to-do items and print the results. Lastly, it features a custom category system for containing different lists of tasks.

Since the app provides a lot more functionality, naturally it’s UI is a lot more advanced allowing the user to even drag and drop items along with having dialog boxes in different parts of the app.

The application has several static pages providing helpful information to the user about many of its features and how to best use the app. It also allows the user to reset their data to a time back when they first starting using the application.

#### Tests

The app is built using the jQuery library. It also shows ads to its users on the right side. These ads increase the load-time for the website especially on Slow 3G Network speeds as shown later in an example below.

The competitor app has a lot of features that don’t exist in our app. So, a small number of features were selected from it that are similar to our app for comparison.

The audit shows that the application's JavaScript code is quite slow when compared to our app. In our test, we found that the application takes a considerably longer amount of time to add a to-do task when compared to our app. As shown in the example screenshots below.

**Average Results:**

- Application Size: 2.4 MB
- DOMContentLoaded: 1.78s
- Memory Heap: 5.8 MB

##### Application Performance on Page Load - Normal Network Speed

- Scripting: 908ms
- Rendering: 146ms

Scripting took the majority of the applications load-time after the completion of the network requests. 

![](screenshots/competitor/01-Page-Load-Online-Network.png)

##### Page Load – Slow 3G Network Speed

![](screenshots/competitor/02-Page-Load-Slow-3G-Network.png)

##### Network – Normal Speed

![](screenshots/competitor/03-Network-Online.png)

##### Network – Slow 3G Speed

![](screenshots/competitor/04-Network-Slow-3G.png)

##### Memory Allocation

![](screenshots/competitor/05-Memory-Allocation.png)

#### Application Use

The general usage tests for the application show that the app continues to use significantly more resources and memory while operating much slower than our app.

**Adding a single task:**

- Scripting: 118ms
- Rendering: 43ms

![](screenshots/competitor/06-Adding-a-single-task.png)

**Marking a task to be completed:**

- Scripting: 206ms
- Rendering: 147ms

![](screenshots/competitor/07-Marking-a-task-to-be-completed.png)

**Navigating between different views:**

- Scripting: 170ms
- Rendering: 82ms

![](screenshots/competitor/08-Selecting-different-filter-view.png)

As outlined by the results above for the competitor’s application. It is significantly slower, uses more memory, and takes a lot more time when reacting to user actions in the UI regarding the lists data.

### Take Away

Our application is a relatively simple app built around the MVC design pattern using vanilla JavaScript. Though, the current implementation is an overkill for such a simple application. Even though the current app is fast and uses few resources when compared to the competitor’s application. It would be a good idea to refactor the app using a 3rd-party library or framework such as ReactJS.

Even with the small hit of having additional dependencies. It would still benefit the app by simplifying the project structure and providing a consistent development experience and good coding practices and patterns already established in the development community.

This should make it easier to hire additional developers down the road while adopting good coding practices such as not using global var variables, immutable data, pure function, etc. Allowing for a solid foundation to build our app on and expand it with additional functionality.